\section{PERFORMANCE EVALUATION}

We implement our fall detection system and deploy it in different room environments for performance evaluation.
Our goal is twofold.
First, we aim to examine the effective detection range and  the accuracy of fall detection via acoustic sensing and also to show its capabilities of distinguishing fall accidents from other human activities.
Second, we conduct various experiments under different environments  in order to show the robustness of our system.



\subsection{Experiment Setup}
\begin{table}
	\caption{Activities types}
	\begin{tabular}{lll}
		\toprule  
		Fall activities&  Fall-like activities& Daily activities\\
		\midrule
		Lose balance-Forward&  Jump&  Walk\\
		Lose balance-Backward&   Sit-down on floor&  Picking up\\
		Lose balance-Left&  Sit-down on chair&  Open the door\\ 
		Lose balance-Right&  Stand-up from floor&  Eat  food\\
		Trip-Forward&  Stand-up from chair&  Drink  water\\
		Trip-Backward&  Raise hand&  Working on computer\\
		Trip-Left&  Picking up\\
		Trip-Right&  Large objects falling\\
		Slip-Forward\\
		Slip-Backward\\
		Slip-Left\\
		Slip-Right\\
		Lose consciousness-Forward\\
		Lose consciousness-Backward\\
		Lose consciousness-Left\\
		Lose consciousness-Right\\	
		\bottomrule
	\end{tabular}
	\label{Fall_types}
\end{table}
Our system is implemented on a speaker (Edifier R1280DB) and a microphone (SAMSON MeteorMic, 16 bit, 48 kHz), which are binding together as one device  placed on the desk, with the speaker generating and emitting signals at 20kHz.
The microphone's sampling frequency is set to 48kHz.
The recorded Doppler signals are sent to a laptop for further analyses, with Matlab employed as the signal processing and machine learning tools.
We set the transmission power to the 80\% of speaker's maximum power and then measure the sound pressure at 1m away from the speaker, to get 45dB.

We conducted our experiment in four different environments: 1) a living room in an apartment  with several tables and chairs,
2) a lab with tables and chairs, 3) a meeting room with a long  table and dozens of chairs around, and 4)  a long corridor.

\subsection{Data Collection}


Since the fall event may cause potential risks to elders, we only recruit 6 elders (5 males and 1 female) to participate.
Besides, 17 young people are also recruited for mimicking the elders' behaviors. 
Hence, there are a total of 23 participants, whose ages range from 60 to 75 for elders and from 24 to 40 for young people.
Among them, there are 17 males and 6 females.
Notably, a mattress is provided for protection from being injured when they fall down. 
For each participant, we collect a continuous stream of activities, mixing the fall, fall-like, and other daily activities as listed in Table~\ref{Fall_types}.
We ask the elders to walk in their nature ways and then fall on the mattress, for collecting their fall events.
All participants are asked to perform the fall actions according to their experiences for simulating  1) the sudden loss of balance, including losing balance, losing consciousness, trip, and slip, and 2) fall forward, backward, and sideward.
In addition, we also ask them to perform some non-fall activities and daily activities (see Table~\ref{Fall_types}). 
For young people, we let them mimic elders' behaviors as follows.
First, we ask them to walk at a slow speed (i.e., less than 1m/s) when collecting the continuous data, because \cite{adam2021association} shows elders with a slow gait speed have a high risk of falling down, as expected due to the loss of muscle strength and mass~\cite{volpato2012role}. 
Also, we ask them to try their best to mimic the falls of elders.
Second, we ask them to do some fall-like activities and daily activities to mimic the elders' behaviors; for example,  sitting down and standing up slowly, while supporting the body by hand.
All collected data are labeled manually, serving as the ground truth.




\subsection{Evaluation Metrics}



We define the following evaluation metrics for measurement.
\begin{itemize}
	\item {\em Accuracy:} which is defined as the ratio of correctly identified samples over all samples, i.e., {\em Accuracy $=\frac{\mathrm{TP}+\mathrm{TN}}{\mathrm{TP}+\mathrm{TN}+\mathrm{FP}+\mathrm{FN}}$}.
	\item {\em Precision:} which is the ratio of the correctly detected falls over all detected falls, i.e.,  {\em Precision $=\frac{\mathrm{TP}}{\mathrm{TP}+\mathrm{FP}}$}.
	\item {Recall:} which means the ratio of correctly detected falls over the total falls. i.e.,  {\em Recall $=\frac{\mathrm{TP}}{\mathrm{TP}+\mathrm{FN}}$}.
	\item {\em F1-score:}, which  reflects the overall performance of the classifier, defined as {\em $\mathrm{F} 1$ score $=\frac{2 \times \text { Recall } \times \text { Precision }}{\text { Recall }+\text { Precision }}$}. 
\end{itemize}
Here, TP represents the true positive, which is the correctly detected falls.
TN represents the true negative, which is the correctly detected non-falls.
FP represents the false positive, which refers to non-falls but wrongly detected as falls.
FN is the false negative, denoting falls but wrongly detected as non-falls.





\subsection{Effective Detection Range}


\begin{figure*}%[!htb]
	\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/_range.pdf}
		\caption{Detection Range.}
		\label{Distance}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/old.pdf}
		\caption{Evaluation on Elders.}
		\label{old}
	\end{minipage}
	\hspace{1em}
\end{figure*}



We test the working range of the system in this experiment, by letting a participant perform both fall and non-fall activities at different distances to the device. 
We collect 100 fall events from $1m$ to $5m$ as the training data to train a HMM.
Then, another 100 fall events and 100 non-fall events are collected at $1m, 2m, 3m, 4m,$ and $5m$, with each distance collecting 20 falls and 20 non-falls.
Figure~\ref{Distance} shows the performance of our system in terms of precision and recall at various distances. 
Detection performance  degrades when the distances from person to the sensing device increase.
This is due to signal attenuation, where the signals reflected from the longer distance will result in the low SNR, making the feature extraction tasks much harder. 
At 5 meters, our precision can still maintain more than 80\%.
The recall stays higher than $95\%$ up to 4 meters.
Even when a person moves to 5 meters away, our system can still maintain  80\% recall for  fall detection.
In the following experiments, we set the distance to 3 meters.

\subsection{Performance on Elders}

In this experiment, we evaluate our system on 6 old participants.
We train a model based on the fall data collected from 2 young participants and then collect a set of fall, fall-like and daily activities for testing from the elders.
In addition, we also collect data from the 2 young participants for testing.
Figure~\ref{old} shows our system performance in terms of accuracy, precision, recall, and F1 score.
From this figure, we observe our system can achieve very high performance for detecting the fall from elders, with the accuracy, precision, recall, and F1 score of 96\%, 95\%, 95\%, and 95\%, respectively. 
The performance on the young participants is even better, to be 97\%, 98\%,95\%, and 96\%, respectively.
Such results show that our system could reliably detect falls from elders.
In addition, it also demonstrates that our system does so, without collecting elders' data for training (in the purpose of protecting elders), amenable to real-world deployment.



\subsection{Performance under Different Environments and Different People}
\label{baseline}
 \begin{figure*}%[!htb]
	\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/_diff.pdf}
		\caption{Impact of different environments and different participants.}
		\label{env}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/los_nlos.pdf}
		\caption{Impact of the nLoS.}
		\label{_nloS}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/object.pdf}
		\caption{Evaluation of the non-fall activities.}
		\label{non-fall}
	\end{minipage}
\end{figure*}



We next  evaluate our system performance under different environments and  users.
In particular, this experiment trains a HMM model only with the 
data collected from two participants in the apartment and then examine the model  on  different participants  under different environments.
To be specific, we conduct four experiments on the 17 younger participants in four different environments: apartment, lab, meeting room, and corridor.
For collection the testing data, the first experiment lets the same participants from the training set to perform activities in the apartment, indicated as E1.
The second experiment asks the same participants from the training set to perform activities in other environments, denoted as E2.
The third experiment lets  participants other than the training set perform  activities in the apartment, indicated as  E3.
The last experiment asks participants  other than the training set to perform activities in other environments, indicated as E4.
Figure~\ref{env} shows the averaged results in each experiment in terms of four metrics.
We can observe our system to always achieve very high performance in the four scenarios (i.e., E1, E2, E3, and E4), with the the accuracy, precision, recall, and F1-Score of $97\%, 97\%, 95\%, 96\%$, of $96\%, 97\%, 93\%, 94\%$, of $95\%, 94\%, 92\%$,  $93\%$, and of $94\%, 94\%, 90\%, 92\%$, respectively.
This set of experiments demonstrate that our trained model at one environment  can be transferred to different environments and different people for use with negligible performance degradation.


\subsection{Impact of Non-Line of Sight (nLoS)}
\label{subsec:nlos}
In the experiments above, we observe that the environment change will have a certain impact on our system performance.
One possible reason is that the blocking of  Doppler signals could  somehow degrade system performance.
Hence, we design another set of experiments to evaluate the impact of the nLoS path and show how to minimize its impact. 
We put a chair between the person and the device to simulate an nLoS scenario.
Three experiments are conducted: 1) the chair is placed at the original place, so there is an  LoS path between the person and the device;  2) the chair is placed on the LoS path, blocking the signals (i.e., nLoS scenario); 3) retraining the model by the nLoS data.
In the first two experiments, the HMM model is trained under the LoS path scenario.
Then, we ask a participant to perform 20 falls and 20 fall-like activities in the first two experiments.
In the third experiment, we ask a student to conduct 60 falls to retrain the HMM model for the nLoS scenario and then evaluate the rebuilt model with data collected in  the first and second scenarios to evaluate its  performance.
Figure~\ref{_nloS} shows the results of three experiments.
From this figure,  a model trained under the LoS path scenario is observed to degrade its  detection performance  greatly when  the chair is moved to the LOS path (from the first experiment to the second experiment), with all  evaluation metrics declines to around 60\% (see the red bar).
This is due to the significant  change of Doppler signals and the reduction of SNR caused by the obstacle. 
However, we found that if the model was reconstructed using the training data collected in the nLoS setting (i.e., the third experiment), the  results restore to a comparable level, with all  performance metrics  recovering to around 90\% (see the green bar).
In addition, we also observe that when applying our model trained on the nLoS setting to test data  from the  first scenario (LoS sample), all  metrics can still reach around 90\% (see the gray bar).
This experiment indicates that our system could eliminate the impact of nLoS path by retraining the model with the nLoS data.
It  gives us the insight that we can collect a rich set of training data to cover all scenarios for maintaining the robustness of our system.






 \begin{figure*}%[!htb]
\hspace{0.5em}
%	\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/_noise.pdf}
		\caption{Impact of the ambient sound.}
		\label{_noise}
	\end{minipage}
\hspace{0.5em}
%\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/devices.pdf}
		\caption{Impact of the devices.}
		\label{devices}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/Different_time.pdf}
		\caption{Impact at different time.}
		\label{time}
	\end{minipage}
\end{figure*}

\subsection{Evaluation of Non-fall Activities}
This section presents experiments to demonstrate system's performance in detecting different types of non-fall activities.
Three types of non-fall activities are experimented: (a) objects ({\em i.e., ball, book, and box}) falling; (b) fall-like activities; and (c) some daily activities.
In (a), an object is falling from a high place.
In (b) and (c), the detailed fall-like and daily activities are shown in Table~\ref{Fall_types}.
For each activity type, we collect 60 samples for 40 fall activities in each experiment.
We employ the model trained from two young people, as in Section~\ref{baseline}.  
Our performance results are illustrated in Figure~\ref{non-fall}, where the purple, red, and green bars correspond respectively to the experiments for (a), (b), and (c).
From this figure, we see our system to always achieve high accuracy in distinguishing these non-fall activities, with all metrics greater than 90\%.
Specifically, it has the highest performance in recognizing fall occurrences from the object falling.
The reason is that the trajectory of any object falling is close to a straight line in the vertical direction, whereas that of a human's fall consists of a series of limbs' movements in different directions, causing a more complex Doppler signal. 
In addition, the sizes of these objects are relatively small comparing to that of a human body, resulting in their generated Doppler signals being much weaker. 
As a result, human fall accidents can be easily distinguished from the falls of those objects.



\subsection{Impact of Time}
We built a model based on the training data collected in November 2020, and then evaluated its performance for detecting human falls in November, December, January, and February.
Notably, the worn clothes vary in different months due to the weather changes.
We collect 40 fall events and 40 non-fall activities in each month for evaluation.  
Figure~\ref{time} shows the precision and recall at the four months. 
From this figure, we observe that the performance of our system changes only slightly across different months.
This experiment demonstrate that the impact of clothes is negligible. 





\subsection{Impact of Ambient Sound}

Since our system relies on acoustic signals, we evaluate the impact of ambient sound on our system. 
We conduct the experiments in the environment when playing music or having human talk.
The sound sources are at $0.5m$ away from the device. 
The music volume is the same as that used for sending ultrasonic signals in our system, and people talk in the normal voice. 
Figure~\ref{_noise} depicts the performance of our system in the two scenarios for comparison with the scenario of no ambient sound (i.e., silent). 
Theoretically, the audible noise should not affect our system, since they are operating at different frequency bands.
However, the figure reveals that the audible noises still have certain impacts on our system, despite slightly.
This is due to the frequency leakage, which results in additional frequency components of the noises spreading to around 20kHz and  mixing  with the resulting Doppler signals of falls.
Since the frequency leakage is not ubiquitous in the  signals, its impact to our system is not severe.



\subsection{Impact of Different Directions}
We also ask one participant to fall in different directions for evaluating our system. 
Specifically, we make the participant  fall in the 0°, 90°, -90°, and arbitrary degrees.  
The 0° means the participant falls along the line right in front of  the speaker, whereas 90° and - 90° mean the fall directions are perpendicular to the sound propagation direction.
Figure~\ref{direction} shows the performance of our system in the four experiments.
Our system is observed to perform the best along the 0° direction, with its accuracy, precision, recall, and F1 score equal to 98\%, 97\%, 97\%, and 97\%, respectively.
When falling in an arbitrary direction, the four metrics drop to   92\%, 92\%, 91\%, and 91\%, respectively.
Notably, when the fall direction is strictly perpendicular, one may assume no Doppler signals to be generated so our system becomes inoperative.
However, our experiment finds that strong Doppler signals are still generated, and our system can achieve the accuracy, precision, recall, and F1 score of around 91\%, 85\%, 90\%, 87\%, respectively.
This could be due to the complex moving patterns of human falls. 
For example, the arm swing and the body movement would generate strong signals.
Also, due to the relative short-range between the device and  the participant (within a room), the sound waves are not confined to one plane. 
Instead, there is always an angle between the fall motion and the wave surface, resulting in a strong Doppler signal. 


\subsection{Transferability of Our System}

We next evaluate our system with three different speakers, i.e., Edifier R1280DB, Logitech z200, and Amazon Echo, denoted as Speaker 1, Speaker 2, and Speaker 3.
The first speaker is our default speaker, which is used for collecting the training data. 
We tune their volumes to the  same transmission power and let them emit the signals at the same frequency, i.e., $20kHz$. 
The performance of our system with these three speakers are shown in Figure~\ref{devices}.
We observe that Speaker 1 achieves the best performance whereas Speaker 3 achieves the worse on all four metrics.
The reason is that Speaker 3, i.e., Amazon Echo, is a home speaker, which is not designed for generating the ultrasonic signal, emitting unstable signals on the high frequency band.
However, our system with Amazon Echo still exhibits the accuracy, precision, recall, and F1 score of 87.5\%,  85.7\%,  90\%,   and  87.8\%, respectively.
This experiment demonstrates the transferability of our system with different  devices.




 \begin{figure*}%[!htb]
	\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/_direction.pdf}
		\caption{Impact at  different directions.}
		\label{direction}
	\end{minipage}
	\hspace{0.5em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/_frequency.pdf}
		\caption{Impact of the frequency.}
		\label{frequency}
	\end{minipage}
\end{figure*}


In addition, we also conduct experiments to show model transferability across different frequencies.
In particular, our experiment evaluates three working frequencies, i.e.,19kHz,20kHz, and 21kHz.
We collect the training data only on the 20kHz, while conducting the detection tasks under  19kHz, 20kHz, and 21kHz.
Figure~\ref{frequency} shows the performance of our detection tasks on the three frequencies. 
This figure reveals that, when comparing to that on the 20kHz,  the performance outcomes of 19kHz and 21kHz degrade only slightly for the detection tasks.
Specifically, the accuracy, precision, recall, and F1 score  are 94\%, 95\%, 90\%, and 93\% ( or 93\%, 92\%, 90\%, and 91\%), respectively, on 19kHz (or 21kHz).
This experiment indicates that our system trained on one frequency can be generalized for use under other frequencies.




\subsection{Computational Cost}
We next measure the processing time of each main component in our system. 
Since our system aims to work in a real-time manner by processing the time sequence signals, we only need to  take  one sliced signal (over 0.8s) for measurement. 
The major computational costs come from the STFT and Interference Cancellation (Denoise) in signal processing, feature extraction, SVD decomposition, and HMM classification, which are $21ms,20ms, 6.7ms, 3.2ms,$ and $1.2ms$, respectively.
The computational costs from other components are negligible and can be omitted. 
Hence, the total processing time for each sliced signal is only around $52.1ms$, deemed to be much less than the moving step length of the HMM window (= $0.1s$).
This experiment validates that our system could support the real-time processing for fall accident detection. 






	
	
