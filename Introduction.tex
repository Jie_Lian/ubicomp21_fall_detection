\section{Introduction}
Fall is one of the most common cause of serious injury for elders.
Each year, 3 million older people would be treated in hospitals  due to falls \cite{ballesteros2017review}, in which fractured bones and soft tissue injuries caused can lead to the death.
The report from \cite{amin2016radar} has shown that  lying on the floor for a long time after the fall may aggravate the injury.
Given this consideration, the development of effective fall detection systems has attracted growing research attention in recent years, aiming to promptly discover the fall events of elders for needed immediate medical assistance. 
Such a system is important to enable rapid intervention and mitigate possible serious consequences resulting from falls,  desirable for home deployment to protect elders' safety.




Diverse fall detection systems have been developed to serve this purpose for years, and they can be categorized into two types:  wearable detection systems and non-wearable detection systems.
Among wearable systems, a range of solutions have been proposed, based on accelerometers and gyroscopes ~\cite{kwolek2015improving,pierleoni2015high}, RFID ~\cite{chen2010indoor, ruan2015tagfall}, and smartphones~\cite{cao2012falld,abbate2012smartphone}.
However, this line of solutions require the elders to wear the sensors  or carry a smartphone using its built-in sensors to identify the fall events with high accuracy, albeit inconvenient.
Given the elders may forget or be reluctant to wear/carry the sensors/devices, the non-wearable based solutions are more attractive, with a set of solutions being developed based on Wi-Fi, radars, and cameras.
However, several limitations in existence restrict the wide deployment of these solutions.
Specifically, WiFi-based solutions~\cite{tian2018rf,palipana2018falldefi,wang2016wifall,wang2016rt} typically need to analyze CSI signal to extract features for the fall activities, usually calling for hardware level support to  incur high cost and development overhead.
In addition, this category of solutions  occupies the communication channels on 2.4GHz or 5GHz, which competes  for  the channel resources with other Wi-Fi devices to degrade the quality of data communication.
The radar-based solutions \cite{gadde2014fall,rivera2014radar,amin2016radar} is  promising, but they require to purchase an often expensive radar device.
The vision-based solutions \cite{lee2005intelligent, stone2014fall, bian2014fall},  could also achieve high accuracy, but it would raise serious privacy concerns.
The acoustic-based solutions have been proposed in \cite{li2012microphone, shaukat2014daily, popescu2009acoustic}, and they detect the fall by extracting Mel-Frequency Cepstral Coefficients (MFCC) \cite{tiwari2010mfcc}  through the fall sound.
Unfortunately, such  solutions could be  affected adversely by other acoustic sounds  in the environment.




In contrast to existing solutions, we aim to propose a lightweight and convenient solution by leveraging the home audio device to emit the  ultrasonic sound for sensing fall occurrence  events, applicable for home use.
Specifically, we control the speaker of an audio device to generate  20kHz ultrasonic signals inaudible by humans while using the equipped microphone to receive reflected signals for analysis.
Since motions of a human in general will cause the Dopper effect of reflected signals, i.e., the frequency changes corresponding to different human activities to yield certain enclosed patterns, this allows us to detect the fall event through analyzing patterns in the reflected signals.
Indeed,  existing work based on Doppler radar  \cite{gadde2014fall,rivera2014radar,amin2016radar}, 
has demonstrated that  human falls can be detected via analyzing the Doppler signals. 
This inspires us to investigate the  fall patterns  out of the Doppler effect of acoustic signals, distinguishable from other daily activities. 	


Our design relies only on one  speaker and one microphone equipped in a typical audio device to achieve our goal.
The speaker would generate a sequence of 20kHz continuous wave, and the microphone would record the Doppler signals  for analysis.
One key challenge here is that the signals  include responses not only from human falls but also from other human activities or object reflection. 
Thus, it is necessary while challenging to develop a series of signal processing solutions and perform the fine-grained analyses to extract the desirable signal patterns corresponding to falls and the features representing their characteristics.
We first apply Short Time Fourier Transform (STFT) for computing the spectrogram of  received signals and then develop a collection of solutions to remove the interferences caused by different factors, i.e.,  direct transmission, environmental reflection, and system imperfection, resulting in the pure fall signals.
The Power Burst Curve (PBC) is then applied to the  spectrogram for automatically detecting human motions from a sequence of received signals, where the start and the end points of the falls, if any, can be identified to indicate fall duration time.
After that, the effective features are extracted so as to represent the fall patterns.
We continue to apply the Singular Value Decomposition (SVD) to reduce the feature dimension to 1, so as to input to a Hidden Markov Model (HMM) for training.
Notably, the selection of  SVD and HMM methods is mainly due to  their low computational complexity and few parameters involved, since we aim to deploy our system in commercial smart home devices with limited processing capabilities.
For example, the SVD is a simple and quick way for the feature reduction, while HMM has a few parameters but is sufficient to tackle our binary classification problem.
Our system is implemented and deployed in practical house environments for evaluation.
The experimental results demonstrate that our system can achieve  superior performance for fall detection and it is robust to  environmental changes. 


Our contribution can be summarized as follows:

\begin{itemize}	
	\item To the best of our knowledge, we are the first to explore  fall detection via ultrasonic sensing, by leveraging an existing home audio device, requiring no  dedicated devices or the specialized changes on hardware.
	Our system is operated at the 20kHz ultrasonic band, imperceptible to humans and also interference-free to Wi-Fi devices. 
	
	\item We design a series of solutions for effectively processing the acoustic signals and extracting the salient patterns.
	Specifically, a set of interference cancellation solutions are designed to eliminate various noise or interference, for obtaining the desired signals that include only clear human activity patterns.
	We extract a set of effective features and then apply SVD to further reduce their dimensions so  as to retain  the useful information, for inputting to a HMM model to train.
	
	
	\item Extensive experiments are conducted in different environments, demonstrating that our system can achieve  superior performance in detecting the fall and distinguish it from other normal activities.
	In addition, we show the transferability of our system, i.e., the model trained in one environment (or frequency) can be applied directly to other environments (or other inaudible frequencies) without noticeable performance degradation. 
\end{itemize}





