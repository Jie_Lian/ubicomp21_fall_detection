\section{RELATED WORK}
\label{sec:related}
Our work closely relates to two research directions: 1) fall detection and 2) acoustic-based sensing.
We review the state of the arts as follows.

\subsection{Fall Detection}
Fall detection has attracted considerable interests in healthcare in the past decade.
Roughly, the fall detection systems can be classified into two categories: wearable sensor-based systems and non-wearable  systems.


Wearable solutions  are typically based on accelerometers and gyroscopes \cite{kwolek2015improving,pierleoni2015high}, smartphones \cite{cao2012falld,abbate2012smartphone}, RFID \cite{chen2010indoor, ruan2015tagfall}, etc. 
These systems can  work when users who wear/carry specific devices. 
In addition to their inconvenience, wearable-based solutions tend to have limited deployment due to the memory decline of the elders that make them forget to wear/carry the sensors/devices. 
In addition, some older people feel uncomfortable to wear the devices and will be reluctant to use them at home~\cite{zhang2006fall}. 


On the other hand, non-wearable technologies overcome  the aforementioned limitations, enabling  continuous fall monitoring without the need to wear/carry devices.
Different categories of non-wearable solutions have been designed, including camera-based, radar-based, ambient-based, and Wi-Fi-based approaches. 
The camera-based system~\cite{lee2005intelligent, stone2014fall, bian2014fall} leveraged a  camera to capture the photos or video sequences of  human activities and  developed activity classification algorithms for  discovering the fall events.
However, it has been well known that this line of solutions will invade people's privacy, suffer from occlusion, and require intensive computation cost for real-time processing.
The  Doppler radar-based approaches~\cite{gadde2014fall,rivera2014radar,amin2016radar}  can directly measure motion velocity by leveraging  the Doppler frequency relationship, but they require dedicated radar devices that work in high frequency bandwidth to achieve  high resolutions. 
Meanwhile,  ambient-based approaches have been proposed to monitor the sound of fall for detection.
In \cite{popescu2009acoustic, li2012microphone, shaukat2014daily}, the MFCC features of fall sound were extracted and then processed by the machine learning classifier.
However, these solutions rely on audible sounds, which could result from many daily activities, such as playing music, talking, etc.,  to yield degraded performance and be environment-specific.


The Wi-Fi-based sensing has become popular in recent years, with many solutions being proposed for fall detection.
For example, some systems measure the fall based on the changes of the received signal strength indicator (RSSI)~\cite{mager2013fall, kianoush2016device, huang2014zigbee}.
However, such RSSI-based solutions are not easy to  deploy due to the requirement of sensors and the detailed fingerprinting of  environment. 
In addition,  the CSI-based solutions \cite{tian2018rf,palipana2018falldefi,wang2016wifall,wang2016rt} have been proposed, utilizing the Short Time Fourier Transform (STFT) or the wavelet transformation to estimate fast changes in the RF signals.  
For example, WiFall \cite{wang2016wifall} leveraged the  CSI amplitude related time domain features to characterize falls of a single person. 
RTFall \cite{wang2016rt} took into account both CSI amplitude and CSI phase while Falldefi \cite{palipana2018falldefi} extracted a set of  features from the CSI,  to characterize the fall activity.
However, Wi-Fi-based solutions require  certain  hardware changes for receiving the CSI signals.
They also occupy the data communication channels, inevitably causing interference to home Wi-Fi devices.






\subsection{Acoustic  Sensing}
Recently, acoustic sensing has also attracted wide attention, and it can be categorized into two lines of research.
The first research line is device-dependent, requiring users to hold a device for sensing.
Solutions \cite{yun2015turning, mao2016cat, zhang2017soundtrak, wang2019millisonic} have been developed for motion tracking. 
Relying on the low propagation speed of acoustic signals, they could achieve high accuracy.
The core idea is to estimate the phase change of the signals from  the transceiver to calculate the distance.
But the nature of these systems is to track the device movement via acoustic signals, rather than a human activity.


The other research line is device-free, freeing the user from carrying a device for sensing.
Recent works \cite{nandakumar2015contactless, nandakumar2019opioid, song2020spirosonic} have shown that acoustic sensing with the FMCW signals can achieve the high accuracy in terms of respiration sensing with a limited range (i.e., about 1 meter).
However, the FMCW signal is unsuitable  for use in our system.
Notably, our system relies on analyzing  the Doppler shift signals for detecting the fall occurrence. 
Since the FMCW signal continuously swipes its frequency,  the multi-path reflection signals in the room environments will somewhat overlap  with the Doppler signals caused by the fall in the frequency domain.
Hence, it is difficult for our system to remove the reflection signals for acquiring the Doppler signals due to fall events.


Acoustic signal has also been used for gesture recognition \cite{gupta2012soundwave, ruan2016audiogest,wang2020push},
in which the  Doppler shifts are leveraged from inaudible acoustic transmissions for recognizing different hands movement patterns and gestures.
However, such designs require hands to be close to the mobile devices.
Some systems \cite{nandakumar2016fingerio, wang2016device, sun2018vskin} also have been developed  to perform contactless tracking via acoustic signals.
The basic idea of these systems is to generate the special modulated signal such as the OFDM symbols for measuring the distance between the target and the transmitter by the Time of Flight (TOF) or the phase changes of  reflected signals.
However, their sensing ranges are usually limited within one meter, unsuitable for home applications.
A recent work RTrack \cite{mao2019rnn} enables the room-scale hand motion tracking by combining  the  signal from a microphone array with a series of signal processing techniques and an RNN. 
Requiring a microphone array, it is not pervasive at home for AOA measurement, and its performance tends to  be affected by the microphone layout.


In addition, some systems focus on human activity recognition.
For example, covertband \cite{nandakumar2017covertband} enables activity recognition by the inaudible OFDM symbols.
It employed a smart speaker as the transceiver and two microphones  as the receiver for identifying  rhythmic motions such as jumping, pumping arms, or pelvic tilts, by analyzing OFDM symbols' correlation profile.
However, fall detection is not considered in its design.
Separately, other systems \cite{xu2019acousticid, altaf2015acoustic,wang2018gait} can identify human's gait patterns from Doppler signals via extracting a set of features to train a machine learning classifier.
Our design of effective fall detection via acoustic sensing is inspired in part by them.



