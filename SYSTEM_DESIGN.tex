\section{SYSTEM DESIGN}
\label{sec:sys_des}

This section illustrates our design of the fall detection system via inaudible acoustic sensing.
We control the speaker to emit continuous ultrasound signals while letting  the microphone  record the reflected signals.
By conducting a series of fine-grained processing and analyses on the reflected signals, our system can identify the signal of interest and  is capable of extracting the desirable features, realizing the fall detection goal.
Many technical challenges exist in the design of our system, including but not limited to: 


\begin{itemize}		
	\item The received signals contain intensive interference coming from the object reflection, multi-path and/or system defection,  which can substantially distort the desirable signals reflected from the human fall events.
	Sometimes, the energy level of such interference even  exceeds that of the signal of interest.
	It is necessary to develop a collection of solutions to eliminate them for gaining the pure signals resulting from fall events.
	
	
	
	\item To recognize fall events, the effective features should be extracted from the signals.
	However, the fall events are diverse which can happen at different directions, resulting in disparate patterns.
	How to extract a set of features for characterizing all different fall actions with disparate patterns is a challenging task.
	Essentially, we aim to make our detection system be robust to different fall direction changes.
	On the other hand, the fast attenuation of acoustic signals will result in  fall's pattern unapparent, further elevating difficulty in the feature extraction task and thus hindering the development of an effective detection system.
	
	
	
	\item The received signals also contain some from other irrelevant activities, such as walking.
	Typically, the fall mostly happens when a person is walking.
	This brings a high requirement for our detection system, which should be able to correctly detect the onset of a fall in a sequence of received signals.
	In addition, some actions such as sitting down, standing up or jumping also have similar patterns as the fall. 
	How to distinguish them from the fall remains challenging.
	
	
	
\end{itemize} 

\begin{figure*}
	\centering
	\includegraphics[width=5.3in]{Fig/system_design.pdf}
	\caption{The workflow of our fall detection system.}
	\label{systemdesign}
\end{figure*}
Before we elaborate our detailed design, we give an overview of our system, consisting of six key  modules: {\em Sensing, Signal Processing, Motion Locating, Feature Extraction, Feature Reduction} and {\em Classification}, as shown in Figure~\ref{systemdesign}. 
In the {\em Sensing} module, the system programs the built-in speaker in an audio device to emit inaudible acoustic signals and uses its microphone to collect reflected signal for analyses.
In the {\em Interference Cancellation} module, we develop solutions to cancel the interference from  environment reflections and system defects to get a clean spectrogram for analysis.
The {\em Fall Locating} module will apply the Power Burst Curve (PBC) to locate the fall event or other motion events from the signals.
In the {\em Feature Extraction} module, a set of effective features that characterize the fall events will be identified and extracted from the spectrogram for classification.
Then, new solutions based on the SVD decomposition and the K-means algorithm  will be designed to reduce the feature dimension and cluster the data, respectively, for getting rid of the redundant features  and  determining the hidden state numbers in the Hidden Markov model (HMM), required by the {\em Feature Reduction} module.
In the end, the {\em Recognition} module takes the data to train a HMM for classifying the events as falls or non-falls.

\subsection{Sensing}
We program the built-in speaker in an audio device to emit continuous inaudible acoustic signals at 20kHz. 
Once the signal is reflected from the human motion, the Doppler effect will be generated for making the frequency shift.
The microphone is used  to  collect Doppler effect signals with a sampling frequency of 48kHz, which ensures it is twice higher than the highest Doppler shift frequency. 
Notably, it is not necessary to let the speaker keep sensing. 
We can program the speaker to periodically send low power 20kHz ultrasound when there is no human motion in the room. 
Once the microphone senses a certain sound pressure level between 19kHz and 21kHz,  our fall detection system will be triggered to emit the ultrasonic signals with normal power.


After receiving the reflected signals at the microphone, we generate its spectrogram for analysis, via applying the short-time Fourier transform (STFT).
In particular, the sequence of original signals is sliced by a set of small windows,  each with a length of 0.4s while any two consecutive windows have  95\% overlapping.
As such,  a $1s$ signal sequence would be sliced into 50 frames.
Then we multiply  each frame by a Hamming window, and  apply an 8192 point Fast Fourier transform (FFT) on each frame.
That is, we divide each frame into 8192 sub-bands, which produce a frequency resolution about 2.5Hz.
After the above process, we construct a  spectrogram for further analysis.




\subsection{Interference Cancellation}
\label{interference}


Since the  speaker is omnidirectional,  the received signal will  contain not only the desirable signals, but also many interferences.
In our experiments, we observe three types of interferences resulted from 1) direct transmission, 2) environment reflection, and 3) system defects.
The first interference type represents the signals directly transmitted from the speaker to the microphone. 
Since the speaker and the microphone are co-located, such an interference will dominate the spectrogram.
The second interference type is caused by the reflection from the environment objects.
The last one is the noise generated due to the imperfection  of devices.

We notice that the direct transmission and the environmental reflection are static over time.
So, to remove the direct transmission noise, we can simply remove the energy on the spectrogram between 19.99kHz and 20.01kHz.
While the environmental reflection can be considered as the stationary noise, we can remove it  by  using spectral subtraction.
To eliminate it, we enable the microphone periodically to record environmental reflection when no people is moving in the room.
We also notice that such a reflection  varies only slightly with time, so we could apply it to the whole signal in the same environment.
We perform the STFT on the recorded reflections to get the noise spectrogram, and  then subtract it from our spectrogram.
Through the two steps, the direct transmission and the  reflection of static objects could be completely removed.



The system defects will cause a certain level of noise, with one example shown in Figure~\ref{Noisy}, from which we can see noise points spreading on the spectrogram. 
This type of noise appears even  when there is no activity happens.
Our experiments show that the energy level of the system defect can be modeled as a  Gaussian distribution.
We record the amplitude of  noise at high frequencies (higher than 20.6kHz) of the spectrogram and construct a noise histogram of amplitudes.
A threshold $N_t$ needs to be set for removing the noise.
Assume  $\mu$ and   $\sigma$ are the  mean and the standard deviation of the Gaussian approximation of the noise histogram, respectively, then we can calculate them directly from this histogram.
Hence, the threshold $N_t$ can be calculated by:
\begin{equation}
{N}_{t}=\mu+\sigma
\end{equation}
We then apply the threshold $N_t$ to the spectrogram, yielding:
\begin{equation}
S(f, t)=\left\{\begin{array}{ll}
S(f, t) & \text { if } S(f, t) \geq {N}_{t} \\
0 & \text { if } S(f, t)<{N}_{t}
\end{array}\right.
\end{equation}
where $S(f, t)$ represents the  power at frequency $f$ and time $t$ on the spectrogram.
This step will retain the points on the spectrogram with their energy levels greater than the threshold.

Figure \ref{Denoised} shows the spectrogram after the above three-step process, observed to retain only the Doppler signals and demonstrating the effectiveness of our interference cancellation methods. 






\subsection{Motion Locating}

\begin{figure*}%[!htb]
	\centering
	\begin{minipage}{0.40\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/_noisy.pdf}
		\caption{Noisy spectrogram.}
		\label{Noisy}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.40\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/denoised.pdf}
		\caption{Denoised spectrogram.}
		\label{Denoised}
	\end{minipage}
\end{figure*}

After  canceling the interference, we obtain  a clean spectrogram.
Since the spectrogram in general contains information about a sequence of events, with fall occurrences possibly accounting for a small fraction, it is highly desirable to locate the onset of any human motion, if present, from the spectrogram, instead of processing the spectrogram as a whole, to lower the processing time.  Note that a fall event belongs to one kind of human motions, which may also include other activities such as walks, jumps, sit-downs, etc.
Thus, the next key step is to locate the starting and end points of a human motion in the spectrogram.
Considering that the motion involves a set of limb movements, a series of high frequency will result due to the Doppler effect. 
This can be observed as  a power burst on the spectrogram.
The power burst curve (PBC) is then applied to locate the motion, where PBC represents the summation of signal power within a specific frequency range between frequencies $f_{l}$ and $f_{u}$.
Taking the positive frequency (frequency higher than 20kHz) as an example, the Power Burst Curve (PBC) could be represented as:
\begin{equation}
P B C(t)=\sum_{f=f_{l}}^{f_{u}}|S(f, t)|^{2}\ .
\end{equation}
Here $f_{l}=20.01kHz$,  is the lower frequency band after eliminating the direct transmission noise and the $f_{u}$ is the upper frequency band which can be set to $20.6kHz$ or higher.
The energy components in this frequency range include the large reflection from the body.
 On the other hand, we set up a threshold value, i.e.,  $PBC_{th}$, in this frequency range, calculated by
 \begin{equation}
 PBC_{th}=\sum_{f=f_{l}}^{f_{u}} {N}_{t}\ .
 \end{equation}
We sum up the signals in this range and compare the result with $PBC_{th}$; if the summation exceeds this threshold, we  consider the presence of a motion.
The start and  end points of the motion can  be determined accordingly, by identifying the two intersections between PBC and the noise threshold.
We truncate signals between the start point and the end point for further analysis.


\subsection{Feature Extraction for Fall Detection}
\label{feature_extraction}

 \begin{figure*}%[!htb]
	\centering
	\begin{minipage}{0.40\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/fallcurve.pdf}
		\caption{Motion curve caused by fall.}
		\label{fallcurve}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.40\textwidth}
		\centering
		\includegraphics[width=2.0in]{Fig/sitcurve.pdf}
		\caption{Motion curve caused by sitting.}
		\label{sitcurve}
	\end{minipage}
\end{figure*}
Once  a motion event is identified from the spectrogram, we next  identify and extract a set of features representing the fall's characteristics.
We observe that the fall spectrogram includes rich information such as the reflection of different body parts. 
In particular, the unique movement of a fall yields a frequency distribution different from those of other activities.
This allows us to extract a set of features from the spectrogram for characterizing the body movement, in order to detect the fall.
Collectively, we extract three sets of features.


The first set is the  speed feature, including the torso speed curve and the arm speed curve, which can quantify the moving
speed and moving pattern of each body part during the fall.
To compute the speed,  we first calculate the frequency shift caused by the movement of each body part.  
Inspired by the percentile method in Doppler radar~\cite{van2008feature}, we extract the frequency curve as follows:
\begin{equation}
T(f, t)=\frac{\sum_{f_{l }}^{f} S(f, t)}{\sum_{f_{l}}^{f_{u }} S(f, t)}, \label{percentile}
\end{equation}
where $S(f, t)$ represents the Doppler effect energy on the frequency $f$ at a certain time $t$ on the spectrogram, $\sum_{f_{\text {l }}}^{f} S(f, t)$ denotes the cumulative energy associated with the frequency range from $f_{l }$ to $f,$ and $\sum_{f_{l }}^{f_{u }} S(f, t)$ represents the total cumulative energy of the Doppler signal at time t.
Hence  $T(f, t)$ represents the ratio of cumulative energy attributed by the frequency below $f$ over the total Doppler effect energy needed at time $t$ to identify a point in the torso contour. 
Our extensive experiments exhibit that we can set the threshold value of $T(f, t)$  as $30\%,75\%, 95\%$, respectively, to roughly derive the frequency $f$ corresponding to the torso movement, leg movement, and arm movement.
The reason for choosing such three thresholds is that they could  better describe the motion of each body part based on our experiments.
We take a non-fall activity, i.e., sitting, for comparison, as shown in Figure~\ref{sitcurve}.
Comparing Figures~\ref{fallcurve} and \ref{sitcurve}, we observe that although the fall and sitting events have similar movement patterns, their motion curves differ significantly, i.e., maximum positive frequencies of the fall curve and the sitting curve are 200Hz and 100Hz, respectively.
Since the fall occurrence will cause strong power appearing on both the positive frequency (higher than 20kHz) and the negative frequency (lower than 20kHz) sides, we shall calculate the frequency curve on both sides to characterize human motions comprehensively.
As such, we can get a total of  6 frequency curves corresponding to  arm movement, leg movement, and torso movement on both positive and negative sides. 
With the curves, we can apply Eqn.~(\ref{velocity}) to  get the moving speed curves of  torso, leg, and arm.





The second set of features is the extreme ratio curve, calculated by
\begin{equation}
R(t)=\max \left(\left|\frac{f_{+\max }(t)}{f_{-\min }(t)}\right|, \quad\left|\frac{f_{-\min }(t)}{f_{+\max }(t)}\right|\right)
\end{equation}
where $f_{+\max }$(t) represents the max frequency shift above 20kHz, deriving from the  Eqn.~(\ref{percentile}) by setting the threshold to $95\%$.
$f_{-\min }$(t) represents the minimum frequency shift below 20kHz, calculated similarly as $f_{+\max }$(t).
Typically, the extreme ratio curve of a fall accident is sharper,  having a larger peak value than those of other activities.
This is due to  no stationary movement of body parts in a fall event,  so its energy amounts on both sides of the spectrogram are highly asymmetric, resulting in a sharper and highly extreme frequency ratio curve.
Other types of motion such as sitting or standing, often have high and symmetric energy amounts in both positive and negative frequency bands, making the extreme frequency curve more stable.


The third set of features is the spectral entropy, which measures the randomness of the energy distribution on a spectrogram of the fall. 
Considering the quick and sudden motions of a fall event, it usually has a higher entropy than other events at both  positive and negative frequency bands due to high energy fluctuation on the spectrogram. 
We calculate the spectral entropy, denoted by $H$, as follows: 
$$
H\left(t\right)=-\sum_{f=f_{l}}^{f_{u}} p\left(f, t\right) \ln p\left(f, t\right)
$$
where $f_{l}$ and $f_{u}$ are upper and lower frequency bounds. 
$H\left(t\right)$ indicates the spectral entropy at a certain time $t$.
$p\left(f, t\right)$ is the normalized power spectral density at  frequency $f$ and time $t$, calculated by:
$$
p\left(f, t\right)=\frac{{P}\left(f, t\right)}{\sum_{f=f_{l}}^{f_{u}} P\left(f, t\right)}
$$ 
where $P\left(f, t\right)$ is the Power Spectral Density, expressed by
$$
{P}(f, t)=\frac{1}{f_{u}-f_{l}} \sum_{f=f_{l}}^{f_{u}} |S(f, t)|^{2}\ ,
$$
where $\sum_{f=f_{l}}^{f_{u}} |S(f, t)|^{2}$ represents the cumulative sum of square for energy attributed by the frequency between $f_{l}$ and  $f_{u}$.





\subsection{SVD Algorithm}
 
Due to the corresponding movement of the entire body, some  features are highly correlated, so they  may not contribute useful information for our fall detection task.
We can further apply singular value decomposition (SVD) to get rid of the redundant features and reduce their dimensions.
Here, SVD aims to reduce the feature dimension  to 1 before inputted to HMM, which will be elaborated in Section~\ref{sec:HMM}.

We assume 0.8s data are inputted each time to HMM.
We extract 8 feature series in the above step, and all of them have the same time resolution as our STFT, which is 0.02s.
As such, we can get an 8$\times$40 feature matrix as the input, denoted by $F$.
The SVD decomposition algorithm can be represented by:
\begin{equation}
F=U \Sigma V^{T}, \label{SVD}
\end{equation}
where $\Sigma$ is an $8 \times 40$ matrix, with all its elements off the main diagonal to be 0. 
Each element on the main diagonal is called a singular value.
%Where $F$ is the original feature with the size of 8 $\times$ 40. 
$U$ and $V$ are unitary matrices, with their sizes of  8$\times$8 and  40$\times$40, respectively.
To compute  $U$, we multiply $F$ and $F^{T}$, to get an 8$\times$8 matrix.
Then we apply the Eigenvalue Decomposition to $F F^{T}$:
\begin{equation}
\left(F F^{T}\right) u_{i}=\lambda_{i} u_{i}\ ,
\end{equation}
where we  can get 8 eigenvalues of matrix $F F^{T}$ and their corresponding 8 eigenvectors $u$. 
All the eigenvectors of  $F F^{T}$ are transformed into 8$\times$8 matrices, which together denote the $U$ matrix given by  Eqn.~(\ref{SVD}).

To calculate $V$,  we multiply $F^{T}$ and $F$ to obtain a 40$\times$40 matrix.
Then, we  apply the eigenvalue decomposition to $F^{T} F$, yielding
\begin{equation}
\left(F^{T} F\right) v_{i}=\lambda_{i} v_{i}\ .
\end{equation}
We  can get 40 eigenvalues of matrix $F^{T} F$ and their corresponding 40 eigenvectors $v$. 
The 40 eigenvectors $v$ are combined with the $V$ matrix  expressed by  Eqn.~(\ref{SVD}).


We next compute  $\Sigma$, which has non-zero values only on its  main diagonal, including the singular value $\sigma$.
With the unitary matrices of $U$ and $V$, we have
\begin{equation}
\begin{split}
F=U \Sigma V^{T} \Rightarrow F V=U \Sigma V^{T} V \Rightarrow F V=U \Sigma \\ \Rightarrow F v_{i}=\sigma_{i} u_{i} \Rightarrow \sigma_{i}=F v_{i} / u_{i}\ .
\end{split}
\end{equation}
Note $\Sigma$  is  arranged in the descending order, so the  reduction of singular values is extremely fast. 
In many cases, the top 10\% or even 1\% of singular value account for more than 99\% of the sum from all singular values. 
In other words, we only need to use the largest $k$ singular values and corresponding left and right singular vectors to approximate the matrix. 
So, we have:
\begin{equation}
F_{m \times n}=U_{m \times m} \Sigma_{m \times n} V_{n \times n}^{T} \approx U_{m \times k} \Sigma_{k \times k} V_{k \times n}^{T}
\end{equation}
If $k=2$,  we can convert $F_{8 \times 40}$ to $F_{2 \times 40}$.
By selecting the first line, we reduce the feature dimension  to 1.
Through this way, we have compressed the multi-dimension features to a 1-D sequence.

We divide $F$ into 10 data units and then calculate the averaged value on each unit to get the observation sequence ${O}$ with length 10.
Then we input ${O}$ to  HMM for further classification.

\subsection{Data Cluster}
We next cluster the data, to determine how many hidden states contained in a fall, so as to further improve the performance of our detection system.
The number of  clusters is equal to the number of hidden states in  HMM.
In our design, we employ the K-means  algorithm~\cite{krishna1999genetic} to cluster the features  extracted from the fall spectrogram analytic process.
To be specific, we first randomly set a center for each cluster. 
Then the distances of each feature vector to those centers are calculated.
We associate each vector with one cluster that has the nearest center and iteratively execute  this step until a convergence.
In the end, the number of clusters equals the number of hidden states in HMM.
Our experiments found 4 clusters, representing the number of invisible states in human fall events, such as: balance state, losing balance state, impact state, and stable state after the impact.

\subsection{HMM (Hidden Markov Model)}
\label{sec:HMM}
 HMM had been widely applied to speech recognition \cite{abdel2012applying, palaz2019end} and to the medical purpose \cite{yang2017medical, pan2013heartbeat}.
Its successful application in this field proves the ability of HMM to process biological sequence signals.
Although the human motion-induced Doppler signal is  very complex and dynamic  with many different features, we can transform it to a simple feature sequence via our SVD algorithm.
Then we  apply HMM to process such a sequence to analyze the state transition indirectly through the feature sequence extracted from the Doppler signals. 



We denote  the HMM that represents the fall process as $\lambda$, yielding: 
$$
\lambda=( A, B, \pi)\ .
$$ 
where $A$ is the state transition matrix,  $B$ is the Emission matrix, and $\pi$ is the initial state distribution.
The number of observation values is 10, equal to the length of  input $O$.


To train $\lambda=(A, B, \pi)$, the Baum-Welch algorithm~\cite{baggenstoss2001modified} is applied. 
We first randomly initialize the parameters of  $A, B, \pi$.
Then the forward  and  backward procedure would calculate the expected hidden states according to the observed data $O_{fall}$ and the parameters of  $A, B, \pi$.
We update the parameters to best fit the observed data $O_{fall}$ and the expected hidden states.
These steps repeat until  the parameters are converged.
After being trained,  $\lambda=(A, B, \pi)$ can describe the features of a fall process. 
With the trained HMM (i.e., $\lambda$) and a new observation series $O_{new}$ acquired from any motion process, the conditional probability $P(O_{new} \mid \lambda)$ could represent the marching degree of $\lambda$ and $O_{new}$. 




\subsection{Classification}

In the feature series, we set the input length of HMM as 0.8s and the moving step length of the window as $0.1s$. 
We slide a 0.8s data window on the signals, according to the method described in Section \ref{feature_extraction}  for feature series extraction.
Then, we apply SVD to the features for dimensional reduction to get a 1-D  sequence, serving as the observation sequence $O_{new}$ of HMM.
$O_{new}$ is inputted into the trained fall process model (Section~\ref{sec:HMM}) for calculating the output probability $P(O_{new} \mid \lambda)$.
This probability is compared to a pre-set threshold $P$: if it is greater than $P$, this sequence represents a fall event; otherwise, it does not. 
Our empirical experiments show that $P$ can be set to $0.156$ for fall detection.




